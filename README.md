# This is a maven plugin example

## how to compile

```
mvn clean install
```

## how to run

Two ways:

1. Create a maven project
2. run : `mvn hk.quantr:test-maven-plugin:1.0:sayhi` or `mvn hk.quantr:test-maven-plugin:sayhi`

**Tips:** version is not required to run a standalone goal.

OR

1. Create a maven project
2. Add these to your pom.xml

```
<build>
	<plugins>
		<plugin>
			<groupId>hk.quantr</groupId>
			<artifactId>test-maven-plugin</artifactId>
			<version>1.0</version>
			<executions>
				<execution>
					<phase>compile</phase>
					<goals>
						<goal>sayhi</goal>
						<goal>sayhello</goal>
					</goals>
				</execution>
			</executions>
		</plugin>
	</plugins>
</build>
```
3. execute `mvn compile`
